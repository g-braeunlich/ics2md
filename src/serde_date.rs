use super::Date;

const DATE_TIME_FMT: &str = "%Y-%m-%dT%H:%M:%S.%3fZ";
const DATE_FMT: &str = "%Y-%m-%d";

pub fn serialize<S>(date: &Date, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    use serde::Serialize;
    match date {
        Date::DateTime(date_time) => date_time
            .format(DATE_TIME_FMT)
            .to_string()
            .serialize(serializer),
        Date::Date(date) => date.format(DATE_FMT).to_string().serialize(serializer),
    }
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Date, D::Error>
where
    D: serde::Deserializer<'de>,
{
    use serde::Deserialize;
    let s = String::deserialize(deserializer)?;
    match s.len() {
        24 => Ok(Date::DateTime(
            chrono::NaiveDateTime::parse_from_str(&s, DATE_TIME_FMT)
                .map_err(serde::de::Error::custom)?
                .and_utc(),
        )),
        10 => Ok(Date::Date(
            chrono::NaiveDate::parse_from_str(&s, DATE_FMT).map_err(serde::de::Error::custom)?,
        )),
        _ => Err(serde::de::Error::custom(format!(
            "Invalid date / datetime: {s}"
        ))),
    }
}
