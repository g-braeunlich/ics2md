mod ics;
mod serde_date;
#[cfg(test)]
mod testing;

use clap::Parser;

/// Convert an ics calendar to a folder with md files
#[derive(clap::Parser, Debug)]
#[clap(about)]
struct Cli {
    #[clap(long)]
    md_folder: std::path::PathBuf,
    #[clap(long)]
    calendar_url: String,
    #[clap(long)]
    exclude: Vec<String>,
}

#[derive(Debug, Default, PartialEq, serde::Deserialize, serde::Serialize)]
struct Event<P = ()> {
    #[serde(default)]
    uid: Option<String>,
    #[serde(skip)]
    title: String,
    #[serde(with = "serde_date")]
    start: Date,
    #[serde(with = "serde_date")]
    end: Date,
    #[serde(skip)]
    description: Option<String>,
    #[serde(default)]
    location: Option<String>,
    #[serde(default)]
    organizer: Option<String>,
    #[serde(skip)]
    md_path: P,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    tags: Vec<String>,
}

#[derive(Debug, PartialEq)]
enum Date {
    Date(chrono::NaiveDate),
    DateTime(chrono::DateTime<chrono::Utc>),
}

impl Default for Date {
    fn default() -> Self {
        Self::DateTime(Default::default())
    }
}

fn main() -> Result<(), Error> {
    let opt = Cli::parse();
    let exclude = opt.exclude.into_iter().collect();
    sync(&opt.calendar_url, &opt.md_folder, &exclude)?;
    Ok(())
}

impl Event<std::path::PathBuf> {
    fn slug(&self) -> Option<&str> {
        self.md_path
            .file_name()
            .and_then(|f| f.to_str())
            .and_then(|f| f.strip_suffix(".md"))
    }
}

fn import_md_folder(
    path: &std::path::Path,
    exclude: &std::collections::HashSet<String>,
) -> Result<Vec<Event<std::path::PathBuf>>, Error> {
    let paths = std::fs::read_dir(path)?;
    let events = paths
        .filter_map(|p| p.ok())
        .filter(|e| {
            e.path().extension().is_some_and(|ext| ext == "md")
                && e.file_name().to_str().is_some_and(|f| !exclude.contains(f))
        })
        .map(|p| import_md(&p.path()))
        .collect::<Result<Vec<_>, _>>()?;
    let mut uids = std::collections::HashSet::new();
    let mut duplicates = Vec::new();
    for evt in &events {
        if let Some(uid) = &evt.uid {
            if uids.contains(uid.as_str()) {
                duplicates.push(uid.as_str());
            }
            uids.insert(uid.as_str());
        }
    }
    if !duplicates.is_empty() {
        eprint!("⚠️ Duplicate event uids: {}", duplicates.join(", "));
    }
    Ok(events)
}

#[derive(Debug, Default, serde::Deserialize, serde::Serialize)]
struct MdEvent<S, E> {
    title: S,
    extra: E,
}

fn import_md_from_str(
    content: &str,
    md_path: std::path::PathBuf,
) -> Result<Event<std::path::PathBuf>, Error> {
    let Some((frontmatter, body)) = content
        .strip_prefix("+++\n")
        .and_then(|s| s.split_once("+++\n"))
    else {
        return Err(format!("{md_path:?}: Missing frontmatter").into());
    };
    let event: MdEvent<String, Event<std::path::PathBuf>> = toml::from_str(frontmatter)?;
    let description = body.trim();
    let description = (!description.is_empty()).then_some(description.to_string());
    Ok(Event {
        md_path,
        description,
        title: event.title,
        ..event.extra
    })
}

fn import_md(path: &std::path::Path) -> Result<Event<std::path::PathBuf>, Error> {
    let content = std::fs::read_to_string(path)?;
    import_md_from_str(&content, path.to_path_buf())
}

fn export_md_to_writer(event: &Event, writer: &mut impl std::io::Write) -> Result<(), Error> {
    let prelude = toml::to_string(&MdEvent::<&str, &Event> {
        extra: event,
        title: event.title.as_str(),
    })?;
    let prelude = prelude.trim_end();
    write!(
        writer,
        r#"+++
{prelude}
+++
"#
    )?;
    if let Some(description) = &event.description {
        write!(writer, "\n{}", description.trim())?
    }
    Ok(())
}

fn export_md(event: &Event, path: &std::path::Path) -> Result<(), Error> {
    let mut f = std::fs::File::create(path)?;
    export_md_to_writer(event, &mut f)
}

struct AssignSlug<'a, I> {
    taken_slugs: &'a std::collections::HashSet<&'a str>,
    owned_taken_slugs: std::collections::HashSet<String>,
    iter: I,
}

impl<'a, I: Iterator<Item = &'a Event>> Iterator for AssignSlug<'a, I> {
    type Item = (String, &'a Event);
    fn next(&mut self) -> Option<Self::Item> {
        let event = self.iter.next()?;
        let base = slugify(&event.title);
        let mut slug = base.clone();
        let mut n = 2;
        while self.taken_slugs.contains(slug.as_str()) || self.owned_taken_slugs.contains(&slug) {
            slug = format!("{base}-{n}");
            n += 1;
        }
        self.owned_taken_slugs.insert(slug.clone());
        Some((slug, event))
    }
}

fn assign_slug<'a, I: Iterator<Item = &'a Event>>(
    iter: I,
    taken_slugs: &'a std::collections::HashSet<&'a str>,
) -> AssignSlug<'a, I> {
    AssignSlug {
        taken_slugs,
        owned_taken_slugs: std::collections::HashSet::new(),
        iter,
    }
}

fn slugify(title: &str) -> String {
    title
        .to_lowercase()
        .replace([' ', ',', '.', ':', '/', '\\'], "-")
        .replace("--", "-")
        .replace('ä', "ae")
        .replace('ö', "oe")
        .replace('ü', "ue")
        .trim_matches('-')
        .to_string()
}

fn sync(
    calendar_url: &str,
    md_root: &std::path::Path,
    exclude: &std::collections::HashSet<String>,
) -> Result<(), Error> {
    let ics_events = ics::load_ics(&get(calendar_url)?)?;
    std::fs::create_dir_all(md_root)?;
    let md_events = import_md_folder(md_root, exclude)?;
    let EventDiff {
        new: new_events,
        deleted: deleted_events,
        changes: event_changes,
    } = diff_events(&md_events, &ics_events);

    let mut taken_slugs: std::collections::HashSet<&str> =
        md_events.iter().filter_map(Event::slug).collect();
    for event in &deleted_events {
        std::fs::remove_file(&event.md_path)?;
        if let Some(slug) = event.slug() {
            taken_slugs.remove(slug);
        }
    }
    for (old, new) in &event_changes {
        export_md(new, &old.md_path)?;
    }
    for (slug, event) in assign_slug(new_events.into_iter(), &taken_slugs) {
        export_md(event, &md_root.join(slug + ".md"))?;
    }
    Ok(())
}

struct EventDiff<'a> {
    new: Vec<&'a Event>,
    deleted: Vec<&'a Event<std::path::PathBuf>>,
    changes: Vec<(&'a Event<std::path::PathBuf>, &'a Event)>,
}

fn diff_events<'a>(
    md_events: &'a [Event<std::path::PathBuf>],
    ics_events: &'a [Event],
) -> EventDiff<'a> {
    let reference_uids = md_events
        .iter()
        .filter_map(|evt| evt.uid.as_deref())
        .collect::<std::collections::HashSet<_>>();
    let ics_uids = ics_events
        .iter()
        .filter_map(|evt| evt.uid.as_deref().map(|uid| (uid, evt)))
        .collect::<std::collections::HashMap<_, _>>();
    let new_events = ics_events
        .iter()
        .filter(|evt| {
            evt.uid
                .as_deref()
                .is_some_and(|uid| !reference_uids.contains(uid))
        })
        .collect();
    let deleted_events = md_events
        .iter()
        .filter(|evt| {
            evt.uid.is_none()
                || evt
                    .uid
                    .as_deref()
                    .is_some_and(|uid| !ics_uids.contains_key(uid))
        })
        .collect();
    let changes = md_events
        .iter()
        .filter_map(|evt| {
            evt.uid.as_deref().and_then(|uid| {
                ics_uids.get(uid).and_then(|ics| {
                    (evt.essential_part() != ics.essential_part()).then_some((evt, *ics))
                })
            })
        })
        .collect();
    EventDiff {
        new: new_events,
        deleted: deleted_events,
        changes,
    }
}

#[derive(Debug, PartialEq)]
struct EssentialEvent<'a> {
    title: &'a str,
    start: &'a Date,
    end: &'a Date,
    description: Option<&'a str>,
    location: Option<&'a str>,
    organizer: Option<&'a str>,
}

impl<P> Event<P> {
    fn essential_part(&self) -> EssentialEvent<'_> {
        EssentialEvent {
            title: &self.title,
            start: &self.start,
            end: &self.start,
            description: self.description.as_deref().map(str::trim),
            location: self.location.as_deref(),
            organizer: self.organizer.as_deref(),
        }
    }
}

fn get(url: &str) -> Result<String, Error> {
    Ok(reqwest::blocking::get(url)?.text()?)
}

struct Error(pub String);

impl<T: std::fmt::Display> From<T> for Error {
    fn from(src: T) -> Error {
        Error(format!("{}", src))
    }
}
impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.pad(self.0.as_str())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::testing;
    #[test]
    fn test_diff_events_detects_deleted_event() {
        let original_events = vec![testing::Evt {
            uid: "uid-a",
            title: "Event A",
            start: "2023-02-20T12:00:00Z",
            end: "2023-02-20T15:00:00Z",
            description: Some("..."),
            location: Some("Area 51"),
            organizer: Some("Chuck Norris"),
        }
        .into()];
        let changed_events = vec![
            testing::Evt {
                uid: "uid-a",
                title: "Event A",
                start: "2023-02-20T12:00:00Z",
                end: "2023-02-20T15:00:00Z",
                description: Some("..."),
                location: Some("Area 51"),
                organizer: Some("Chuck Norris"),
            }
            .into(),
            testing::Evt {
                uid: "uid-b",
                title: "Event B",
                start: "2023-04-15T11:00:00Z",
                end: "2023-04-15T14:00:00Z",
                description: Some("...."),
                location: Some("?"),
                organizer: None,
            }
            .into(),
        ];
        let diff = diff_events(&original_events, &changed_events);
        assert!(diff.deleted.is_empty());
        assert!(diff.changes.is_empty());
        assert_eq!(diff.new, vec![&changed_events[1]]);
    }
    #[test]
    fn test_diff_events_detects_new_event() {
        let original_events = vec![
            testing::Evt {
                uid: "uid-a",
                title: "Event A",
                start: "2023-02-20T12:00:00Z",
                end: "2023-02-20T15:00:00Z",
                description: Some("..."),
                location: Some("Area 51"),
                organizer: Some("Chuck Norris"),
            }
            .into(),
            testing::Evt {
                uid: "uid-b",
                title: "Event B",
                start: "2023-04-15T11:00:00Z",
                end: "2023-04-15T14:00:00Z",
                description: Some("...."),
                location: Some("?"),
                organizer: None,
            }
            .into(),
        ];
        let changed_events = vec![testing::Evt {
            uid: "uid-a",
            title: "Event A",
            start: "2023-02-20T12:00:00Z",
            end: "2023-02-20T15:00:00Z",
            description: Some("..."),
            location: Some("Area 51"),
            organizer: Some("Chuck Norris"),
        }
        .into()];
        let diff = diff_events(&original_events, &changed_events);
        assert!(diff.new.is_empty());
        assert!(diff.changes.is_empty());
        assert_eq!(diff.deleted, vec![&original_events[1]]);
    }
    #[test]
    fn test_diff_events_detects_changed_event() {
        let original_events = vec![testing::Evt {
            uid: "uid-a",
            title: "Event A",
            start: "2023-02-20T12:00:00Z",
            end: "2023-02-20T15:00:00Z",
            description: Some("..."),
            location: Some("Area 51"),
            organizer: Some("Chuck Norris"),
        }
        .into()];
        let changed_events = vec![testing::Evt {
            uid: "uid-a",
            title: "Event A",
            start: "2023-02-20T12:00:00Z",
            end: "2023-02-20T15:00:00Z",
            description: Some("...."),
            location: Some("Area 51"),
            organizer: Some("Chuck Norris"),
        }
        .into()];
        let diff = diff_events(&original_events, &changed_events);
        assert!(diff.new.is_empty());
        assert!(diff.deleted.is_empty());
        assert_eq!(
            diff.changes,
            vec![(&original_events[0], &changed_events[0])]
        );
    }
    #[test]
    fn test_import_after_export_has_no_changes_if_content_is_some() {
        let original_event = testing::Evt {
            uid: "uid-a",
            title: "Event A",
            start: "2023-02-20T12:00:00Z",
            end: "2023-02-20T15:00:00Z",
            description: Some("...\n"),
            location: Some("Area 51"),
            organizer: Some("Chuck Norris"),
        }
        .into();
        let mut buf = Vec::new();
        export_md_to_writer(&original_event, &mut buf).unwrap();
        let output = std::str::from_utf8(buf.as_slice()).unwrap().to_string();
        let imported_event = import_md_from_str(&output, Default::default()).unwrap();
        assert_eq!(
            original_event.essential_part(),
            imported_event.essential_part()
        );
    }
    #[test]
    fn test_import_after_export_has_no_changes_if_content_is_none() {
        let original_event = testing::Evt {
            uid: "uid-a",
            title: "Event A",
            start: "2023-02-20T12:00:00Z",
            end: "2023-02-20T15:00:00Z",
            description: None,
            location: Some("Area 51"),
            organizer: Some("Chuck Norris"),
        }
        .into();
        let mut buf = Vec::new();
        export_md_to_writer(&original_event, &mut buf).unwrap();
        let output = std::str::from_utf8(buf.as_slice()).unwrap().to_string();
        let imported_event = import_md_from_str(&output, Default::default()).unwrap();
        assert_eq!(
            original_event.essential_part(),
            imported_event.essential_part()
        );
    }
}
