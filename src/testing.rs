use super::Event;

pub struct Evt {
    pub uid: &'static str,
    pub title: &'static str,
    pub start: &'static str,
    pub end: &'static str,
    pub description: Option<&'static str>,
    pub location: Option<&'static str>,
    pub organizer: Option<&'static str>,
}
impl<P: Default> From<Evt> for Event<P> {
    fn from(value: Evt) -> Self {
        fn parse_date(s: &str) -> crate::Date {
            match s.len() {
                10 => crate::Date::Date(s.parse().unwrap()),
                20 => crate::Date::DateTime(s.parse().unwrap()),
                _ => panic!("Invalid date"),
            }
        }
        Self {
            uid: Some(value.uid.to_string()),
            title: value.title.to_string(),
            start: parse_date(value.start),
            end: parse_date(value.end),
            description: value.description.map(str::to_string),
            location: value.location.map(str::to_string),
            organizer: value.organizer.map(str::to_string),
            ..Default::default()
        }
    }
}
