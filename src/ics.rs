use super::{Error, Event};
use chrono::Datelike;
use std::str::FromStr;

impl FromStr for TimeDelta {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let negative = s.starts_with('-');
        if s.len() != 5 || !(s.starts_with('+') || negative) {
            return Err(format!("Invalid tzoffset: {s}").into());
        }
        // TODO: use split_at_checked:
        let (h_str, m_str) = s.trim_start_matches(['+', '-']).split_at(2);
        let dt = h_str.parse::<u16>()? * 60 + m_str.parse::<u16>()?;
        Ok(Self {
            value: if negative {
                -i32::from(dt)
            } else {
                i32::from(dt)
            },
        })
    }
}

#[derive(Debug, Default)]
struct TimeDelta {
    value: i32,
}

impl From<&TimeDelta> for chrono::TimeDelta {
    fn from(value: &TimeDelta) -> Self {
        Self::minutes(value.value.into())
    }
}

#[derive(Debug)]
struct RecurrenceRule {
    by_month: u32,
    by_day_day: chrono::Weekday,
    by_day_occurrence: i32,
    until: Option<String>,
}

#[derive(Debug, Default)]
struct TimeZone {
    id: String,
    observances: Vec<Observance>,
}

#[derive(Debug)]
struct BuiltinObservance<'a> {
    rrule: &'a RecurrenceRule,
    tzoffsetto: &'a TimeDelta,
    tzoffsetfrom: &'a TimeDelta,
    start_time: chrono::NaiveTime,
}

#[derive(Debug)]
struct BuiltinTimezone<'a> {
    tzid: &'a str,
    daylight: BuiltinObservance<'a>,
    standard: BuiltinObservance<'a>,
}

const BUILTIN_TIMEZONES: &[BuiltinTimezone<'static>] = &[BuiltinTimezone {
    tzid: "Europe/Zurich",
    daylight: BuiltinObservance {
        rrule: &RecurrenceRule {
            by_month: 3,
            by_day_day: chrono::Weekday::Sun,
            by_day_occurrence: -1,
            until: None,
        },
        tzoffsetto: &TimeDelta { value: 120 },
        tzoffsetfrom: &TimeDelta { value: 60 },
        start_time: time_from_h(2),
    },
    standard: BuiltinObservance {
        rrule: &RecurrenceRule {
            by_month: 10,
            by_day_day: chrono::Weekday::Sun,
            by_day_occurrence: -1,
            until: None,
        },
        tzoffsetto: &TimeDelta { value: 60 },
        tzoffsetfrom: &TimeDelta { value: 120 },
        start_time: time_from_h(3),
    },
}];

const fn time_from_h(h: u32) -> chrono::NaiveTime {
    if let Some(t) = chrono::NaiveTime::from_hms_opt(h, 0, 0) {
        t
    } else {
        panic!("")
    }
}

impl<'a> BuiltinTimezone<'a> {
    fn to_utc(
        &self,
        date_time: chrono::DateTime<chrono::Utc>,
    ) -> Result<chrono::DateTime<chrono::Utc>, Error> {
        let daylight_start = switch_time(
            date_time.year(),
            self.daylight.rrule,
            self.daylight.start_time,
        )
        .map_err(|e| format!("Time zone id '{}', daylight: {}", self.tzid, e.0))?
            - chrono::TimeDelta::from(self.daylight.tzoffsetfrom);
        let standard_start = switch_time(
            date_time.year(),
            self.standard.rrule,
            self.standard.start_time,
        )
        .map_err(|e| format!("Time zone id '{}', standard: {}", self.tzid, e.0))?
            - chrono::TimeDelta::from(self.standard.tzoffsetfrom);
        let tzoffset = if daylight_start <= date_time && date_time <= standard_start {
            self.daylight.tzoffsetto
        } else {
            self.standard.tzoffsetto
        };
        Ok(date_time - chrono::TimeDelta::from(tzoffset))
    }
}

#[derive(Debug, Default)]
struct Observance {
    kind: ObservanceKind,
    dtstart: chrono::DateTime<chrono::Utc>,
    tzoffsetfrom: TimeDelta,
    tzoffsetto: TimeDelta,
    rrule: Option<RecurrenceRule>,
}

#[derive(Debug, Default, PartialEq, Eq)]
enum ObservanceKind {
    #[default]
    Standard,
    Daylight,
}

#[derive(Debug, Default)]
struct VCalendar {
    timezones: std::collections::HashMap<String, Vec<Observance>>,
    events: Vec<EventWithTimeZone>,
}

#[derive(Debug, Default)]
struct EventWithTimeZone {
    event: Event,
    start_tz: Option<String>,
    end_tz: Option<String>,
}

pub fn load_ics(ics_body: &str) -> Result<Vec<Event>, Error> {
    let ics_body = unfold_content_lines(ics_body);
    let mut lines = ics_body.lines().enumerate();
    while lines
        .next()
        .is_some_and(|(_, line)| line != "BEGIN:VCALENDAR")
    {}
    let vcal = TakeUntil::new(&mut lines, "END:VCALENDAR")
        .parse_with(VCalendar::default(), parse_vcalendar)?;
    vcal.events
        .into_iter()
        .map(|evt| {
            Ok(Event {
                start: evt
                    .event
                    .start
                    .into_utc(&vcal.timezones, evt.start_tz.as_deref())?,
                end: evt
                    .event
                    .end
                    .into_utc(&vcal.timezones, evt.end_tz.as_deref())?,
                ..evt.event
            })
        })
        .collect::<Result<_, Error>>()
}

impl super::Date {
    fn into_utc(
        self,
        timezones: &std::collections::HashMap<String, Vec<Observance>>,
        tz_id: Option<&str>,
    ) -> Result<Self, Error> {
        match self {
            Self::Date(_) => Ok(self),
            Self::DateTime(dt) => Ok(Self::DateTime(into_utc(dt, timezones, tz_id)?)),
        }
    }
}

fn into_utc(
    date_time: chrono::DateTime<chrono::Utc>,
    timezones: &std::collections::HashMap<String, Vec<Observance>>,
    tz_id: Option<&str>,
) -> Result<chrono::DateTime<chrono::Utc>, Error> {
    let Some(tz_id) = tz_id else {
        return Ok(date_time);
    };
    let observances = timezones
        .get(tz_id)
        .ok_or_else(|| format!("Time zone id '{}' does not exist", tz_id))?;

    let daylight = observances
        .iter()
        .rev()
        .find(|o| o.kind == ObservanceKind::Daylight && o.dtstart < date_time);
    let standard = observances
        .iter()
        .rev()
        .find(|o| o.kind == ObservanceKind::Standard && o.dtstart < date_time);
    match (standard, daylight) {
        (None, None) => {
            Err(format!("Time zone id '{tz_id}' does not have daylight or standard").into())
        }
        (Some(obs), None) | (None, Some(obs)) => {
            Ok(date_time - chrono::TimeDelta::from(&obs.tzoffsetto))
        }
        (Some(standard), Some(daylight)) => {
            let tz = match (daylight.rrule.as_ref(), standard.rrule.as_ref()) {
                (Some(rrule_daylight), Some(rrule_standard)) => &BuiltinTimezone {
                    tzid: tz_id,
                    daylight: BuiltinObservance {
                        rrule: rrule_daylight,
                        tzoffsetto: &daylight.tzoffsetto,
                        tzoffsetfrom: &daylight.tzoffsetfrom,
                        start_time: daylight.dtstart.time(),
                    },
                    standard: BuiltinObservance {
                        rrule: rrule_standard,
                        tzoffsetto: &standard.tzoffsetto,
                        tzoffsetfrom: &standard.tzoffsetfrom,
                        start_time: standard.dtstart.time(),
                    },
                },
                _ => {
                    BUILTIN_TIMEZONES.iter().find(|tz| tz.tzid == tz_id).ok_or_else(|| format!("Time zone id '{tz_id}' has daylight and standard but a missing recurrence rule"))?
                }
            };
            tz.to_utc(date_time)
        }
    }
}

fn switch_time(
    year: i32,
    rrule: &RecurrenceRule,
    dtstart: chrono::NaiveTime,
) -> Result<chrono::DateTime<chrono::Utc>, Error> {
    if rrule.by_day_occurrence != -1 {
        return Err("Only recurrences with a last weekday of month are supported.".into());
    }
    Ok(
        last_weekday_in_month(rrule.by_month, year, rrule.by_day_day)
            .ok_or_else(|| format!("Invalid date: {}-{}-{}", year, rrule.by_month, 1))?
            .and_time(dtstart)
            .and_utc(),
    )
}

fn last_weekday_in_month(
    month: u32,
    year: i32,
    week_day: chrono::Weekday,
) -> Option<chrono::NaiveDate> {
    let (month, year) = if month == 12 {
        (1, year + 1)
    } else {
        (month + 1, year)
    };
    let after_month = chrono::NaiveDate::from_ymd_opt(year, month, 1)?;
    let actual_weekday = after_month.weekday();
    let n_days = actual_weekday.days_since(week_day);
    let n_days = if n_days == 0 { 7 } else { n_days };
    Some(after_month - chrono::TimeDelta::days(n_days.into()))
}
struct TakeUntil<'a, I> {
    end_token: &'a str,
    end_reached: bool,
    iter: I,
    current_line_number: usize,
}

impl<'a, I> TakeUntil<'a, I> {
    fn new(iter: I, end_token: &'a str) -> Self {
        Self {
            end_token,
            iter,
            end_reached: false,
            current_line_number: 0,
        }
    }
    fn assert_end_reached(self) -> Result<(), Error> {
        if !self.end_reached {
            return Err(format!(
                "{}: Expecting token {}",
                self.current_line_number, self.end_token
            )
            .into());
        }
        Ok(())
    }
    fn parse_with<T, F: Fn(T, &mut Self) -> Result<T, Error>>(
        mut self,
        initial: T,
        parser: F,
    ) -> Result<T, Error> {
        let parsed = parser(initial, &mut self)?;
        self.assert_end_reached()?;
        Ok(parsed)
    }
}
impl<'a, I: Iterator<Item = (usize, &'a str)>> TakeUntil<'a, I> {
    fn parse_with_single<T, P: Fn(T, &str) -> Result<T, Error>>(
        mut self,
        mut initial: T,
        parser: P,
    ) -> Result<T, Error> {
        for (n, line) in self.by_ref() {
            initial = parser(initial, line).map_err(|e| format!("{n}: {}", e.0))?;
        }
        self.assert_end_reached()?;
        Ok(initial)
    }
}

impl<'a, I: Iterator<Item = (usize, &'a str)>> Iterator for TakeUntil<'a, I> {
    type Item = (usize, &'a str);
    fn next(&mut self) -> Option<Self::Item> {
        if self.end_reached {
            return None;
        }
        let (n, line) = self.iter.next()?;
        self.current_line_number = n;
        if line == self.end_token {
            self.end_reached = true;
            return None;
        }
        Some((n, line))
    }
}

fn parse_vcalendar<'a>(
    mut obj: VCalendar,
    lines: &mut impl Iterator<Item = (usize, &'a str)>,
) -> Result<VCalendar, Error> {
    loop {
        let Some((n, line)) = lines.next() else {
            break;
        };
        match line {
            "BEGIN:VTIMEZONE" => {
                let tz = TakeUntil::new(lines.by_ref(), "END:VTIMEZONE")
                    .parse_with(TimeZone::default(), parse_timezone)
                    .map_err(|e| format!("{n}: {}", e.0))?;
                obj.timezones.insert(tz.id, tz.observances);
            }
            "BEGIN:VEVENT" => obj.events.push(
                TakeUntil::new(lines.by_ref(), "END:VEVENT")
                    .parse_with_single(EventWithTimeZone::default(), parse_to_event_with_timezone)
                    .map_err(|e| format!("{n}: {}", e.0))?,
            ),
            _ => {}
        }
    }
    Ok(obj)
}

fn parse_timezone<'a>(
    mut obj: TimeZone,
    lines: &mut impl Iterator<Item = (usize, &'a str)>,
) -> Result<TimeZone, Error> {
    loop {
        let Some((_, line)) = lines.next() else {
            break;
        };
        if let Some(id) = line.strip_prefix("TZID:") {
            obj.id = id.to_string();
            continue;
        }
        match line {
            "BEGIN:STANDARD" => obj.observances.push(Observance {
                kind: ObservanceKind::Standard,
                ..TakeUntil::new(lines.by_ref(), "END:STANDARD")
                    .parse_with_single(Observance::default(), parse_to_observance)?
            }),
            "BEGIN:DAYLIGHT" => obj.observances.push(Observance {
                kind: ObservanceKind::Daylight,
                ..TakeUntil::new(lines.by_ref(), "END:DAYLIGHT")
                    .parse_with_single(Observance::default(), parse_to_observance)?
            }),
            _ => {}
        }
    }
    Ok(obj)
}

fn parse_to_observance(obj: Observance, line: &str) -> Result<Observance, Error> {
    let (key, val) = line
        .split_once(':')
        .ok_or_else(|| format!("Invalid instruction: {line}"))?;
    Ok(match key {
        "DTSTART" => Observance {
            dtstart: chrono::NaiveDateTime::parse_from_str(val, "%Y%m%dT%H%M%S")
                .map_err(|e| format!("DTSTART: invalid datetime: {val}: {e}"))?
                .and_utc(),
            ..obj
        },
        "TZOFFSETFROM" => Observance {
            tzoffsetfrom: val.parse()?,
            ..obj
        },
        "TZOFFSETTO" => Observance {
            tzoffsetto: val.parse()?,
            ..obj
        },
        "RRULE" => Observance {
            rrule: Some(val.parse()?),
            ..obj
        },
        _ => obj,
    })
}

fn parse_to_event_with_timezone(
    obj: EventWithTimeZone,
    line: &str,
) -> Result<EventWithTimeZone, Error> {
    let (key, val) = line
        .split_once(':')
        .ok_or_else(|| format!("Invalid instruction: {line}"))?;
    let (key, args) = key.split_once(';').unwrap_or((key, ""));
    Ok(match key {
        "ORGANIZER" => EventWithTimeZone {
            event: Event {
                organizer: Some(extract_arg("CN", args)?.ok_or("ORGANIZER: Missing argument CN")?),
                ..obj.event
            },
            ..obj
        },
        "X-ALT-DESC" | "DESCRIPTION" => {
            let description = clean_escapes(val);
            EventWithTimeZone {
                event: Event {
                    description: (!description.is_empty()).then_some(description),
                    ..obj.event
                },
                ..obj
            }
        }
        "UID" => EventWithTimeZone {
            event: Event {
                uid: Some(val.to_string()),
                ..obj.event
            },
            ..obj
        },
        "SUMMARY" => EventWithTimeZone {
            event: Event {
                title: clean_escapes(val),
                ..obj.event
            },
            ..obj
        },
        "LOCATION" => EventWithTimeZone {
            event: Event {
                location: Some(clean_escapes(val)),
                ..obj.event
            },
            ..obj
        },
        "DTSTART" => {
            let start_tz = extract_arg("TZID", args)?;
            EventWithTimeZone {
                event: Event {
                    start: parse_date_time(val, start_tz.is_some())
                        .map_err(|e| format!("DTSTART: invalid datetime: {val}: {}", e.0))?,
                    ..obj.event
                },
                start_tz,
                ..obj
            }
        }
        "DTEND" => {
            let end_tz = extract_arg("TZID", args)?;
            EventWithTimeZone {
                event: Event {
                    end: parse_date_time(val, end_tz.is_some())
                        .map_err(|e| format!("DTEND: invalid datetime: {val}: {}", e.0))?,
                    ..obj.event
                },
                end_tz,
                ..obj
            }
        }
        _ => obj,
    })
}

fn parse_date_time(s: &str, with_tz: bool) -> Result<super::Date, Error> {
    Ok(match s.len() {
        8 => super::Date::Date(chrono::NaiveDate::parse_from_str(s, "%Y%m%d")?),
        15 => {
            if !with_tz {
                return Err(format!("Naive datetime without timezone info: {s}").into());
            }
            super::Date::DateTime(
                chrono::NaiveDateTime::parse_from_str(s, "%Y%m%dT%H%M%S")?.and_utc(),
            )
        }
        16 => {
            if with_tz {
                return Err(format!("UTC datetime conflicting with timezone info: {s}").into());
            }
            super::Date::DateTime(
                chrono::NaiveDateTime::parse_from_str(
                    s.trim_end_matches(&['z', 'Z']),
                    "%Y%m%dT%H%M%S",
                )?
                .and_utc(),
            )
        }
        _ => return Err(format!("Invalid datetime: {s}").into()),
    })
}

fn clean_escapes(text: &str) -> String {
    text.replace("\\n", "\n")
        .replace("\\,", ",")
        .replace("\\;", ";")
        .replace("\\:", ":")
        .replace("\\.", ".")
}

fn unfold_content_lines(ics_body: &str) -> String {
    ics_body.replace("\r\n ", "")
}

fn extract_arg(arg_name: &str, args: &str) -> Result<Option<String>, Error> {
    for arg in args.split(';') {
        let (key, val) = arg
            .split_once('=')
            .ok_or(format!("Invalid argument: {arg}"))?;
        if key == arg_name {
            return Ok(Some(val.to_string()));
        }
    }
    Ok(None)
}

fn parse_weekday(s: &str) -> Result<chrono::Weekday, Error> {
    match s {
        "MO" => Ok(chrono::Weekday::Mon),
        "TU" => Ok(chrono::Weekday::Tue),
        "WE" => Ok(chrono::Weekday::Wed),
        "TH" => Ok(chrono::Weekday::Thu),
        "FR" => Ok(chrono::Weekday::Fri),
        "SA" => Ok(chrono::Weekday::Sat),
        "SU" => Ok(chrono::Weekday::Sun),
        _ => Err(format!("Invalid day: {s}").into()),
    }
}

impl std::str::FromStr for RecurrenceRule {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut rule = RecurrenceRule {
            by_month: 0,
            by_day_day: chrono::Weekday::Mon,
            by_day_occurrence: 0,
            until: None,
        };
        for assignment in s.split(';') {
            let (key, val) = assignment
                .split_once('=')
                .ok_or(Error(format!("Syntax error: {}", s)))?;
            match key {
                "BYMONTH" => rule.by_month = val.parse()?,
                "FREQ" => {
                    if val != "YEARLY" {
                        return Err(format!("FREQ={val} not implemented").into());
                    }
                }
                "BYDAY" => {
                    // TODO: use split_at_checked:
                    let Some((n, day)) = (if val.len() > 2 {
                        Some(val.split_at(val.len() - 2))
                    } else {
                        None
                    }) else {
                        return Err(format!("Invalid BDAY value: {val}").into());
                    };
                    rule.by_day_day = parse_weekday(day)?;
                    rule.by_day_occurrence = n.parse()?;
                }
                "UNTIL" => rule.until = Some(val.into()),
                _ => {
                    return Err(format!("{key} not implemented").into());
                }
            }
        }
        Ok(rule)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::testing;
    #[test]
    fn test_load_ics_works() {
        const TEST_ICS_BODY: &str = std::include_str!("../test_data/calendar.ics");
        let events = load_ics(TEST_ICS_BODY).unwrap();
        let expected_events = vec![
            testing::Evt{
                uid: "040000008200E00074C5B7101A82E0080000000099803FAF2558D9010000000000000000100000009CE111456D1E9E45AEAE0CC5E3DCED12",
                title: "Hebelkurs",
                start: "2023-02-20T12:00:00Z",
                end: "2023-02-20T15:00:00Z",
                description: Some("REMINDER"),
                location: Some("Budo Schule Samurai Zürich, Eisfeldstr. 14. 8050 Zürich"),
                organizer: Some("Karl Feierabend")
            }.into(),
            testing::Evt {
                uid: "e24f06ccb85c5a554ce19a859f7f5633745d195a",
                title: "Escrima Lehrgang 2023",
                start: "2023-04-15T11:00:00Z",
                end: "2023-04-15T14:00:00Z",
                description: Some("## Anfahrt\n\nTram 10 - Station Bahnhof Oerlikon Ost, Friesstr. Richtung Seebach\n\nTram 11 - Station Leutschenbach, Neben grossem Parklplatz\nEisfeldstrasse hochgehen\n\n"),
                location: Some("Budo Schule Samurai Zürich, Eisfeldstr. 14. 8050 Zürich"),
                organizer: None
            }.into(),
            testing::Evt {
                uid: "756bdd45-8189-403b-9090-effad4a5f3db",
                title: "Waffenkurs Jo und Katana",
                start: "2023-06-17T11:00:00Z",
                end: "2023-06-17T14:00:00Z",
                description: None,
                location: Some("BUDO SCHULE SAMURAI, Zürich / Eisfeldstr. 14, 8050 Zürich"),
                organizer: None
            }.into(),
            testing::Evt {
                uid: "fdc346de-2165-4770-bae5-51c547dbfb85",
                title: "Osterferien - die Schule ist an diesen Tagen geschlossen",
                start: "2023-04-07",
                end: "2023-04-11",
                description: None,
                location: Some("Budo Schule Samurai, Zürich / Eisfeldstr. 14, 8050 Zürich"),
                organizer: None
            }.into(),
            testing::Evt {
                uid: "0d0a93f0-4fef-4532-bd72-54e2b1baf594",
                title: "Aikido Intensiv Lehrgang mit Sensei Karl Feierabend, 5. Dan",
                start: "2024-11-07T08:30:00Z",
                end: "2024-11-11T15:00:00Z",
                description: Some("9.30 -  11.30 Uhr Aikido Kata\n13.30 - 16.00 Uhr Aikido Gaeshi Waza und Kombinationen"),
                location: Some("Budo Schule Samurai, Zürich / Eisfeldstr. 14, 8050 Zürich"),
                organizer: None
            }.into()];
        assert_eq!(events, expected_events);
    }
    #[test]
    fn test_load_ics_loads_ics_with_incomplete_timezone() {
        const TEST_ICS_BODY: &str = r#"BEGIN:VCALENDAR
BEGIN:VTIMEZONE
TZID:Europe/Zurich
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
DTSTART:20231029T010000
END:STANDARD
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
DTSTART:20230326T010000
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VEVENT
DTSTAMP:20240124T150214Z
SUMMARY:Aikido Intensiv Lehrgang
DESCRIPTION:Intensiv Lehrgang
LOCATION:Budo Schule Samurai\, Zürich / Eisfeldstr. 14\, 8050 Zürich
DTSTART;TZID=Europe/Zurich:20241107T093000
DTEND;TZID=Europe/Zurich:20241111T160000
UID:0d0a93f0-4fef-4532-bd72-54e2b1baf594
END:VEVENT
END:VCALENDAR
"#;
        let events = load_ics(TEST_ICS_BODY).unwrap();
        let expected_events = vec![testing::Evt {
            uid: "0d0a93f0-4fef-4532-bd72-54e2b1baf594",
            title: "Aikido Intensiv Lehrgang",
            start: "2024-11-07T08:30:00Z",
            end: "2024-11-11T15:00:00Z",
            description: Some("Intensiv Lehrgang"),
            location: Some("Budo Schule Samurai, Zürich / Eisfeldstr. 14, 8050 Zürich"),
            organizer: None,
        }
        .into()];
        assert_eq!(events, expected_events);
    }
    #[test]
    fn test_load_ics_cannot_load_ics_with_incomplete_timezone_without_builtin_timezone() {
        const TEST_ICS_BODY: &str = r#"BEGIN:VCALENDAR
BEGIN:VTIMEZONE
TZID:Europe/Berlin
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
DTSTART:20231029T010000
END:STANDARD
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
DTSTART:20230326T010000
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VEVENT
DTSTAMP:20240124T150214Z
SUMMARY:Aikido Intensiv Lehrgang
DESCRIPTION:Intensiv Lehrgang
LOCATION:Budo Schule Samurai\, Zürich / Eisfeldstr. 14\, 8050 Zürich
DTSTART;TZID=Europe/Berlin:20241107T093000
DTEND;TZID=Europe/Berlin:20241111T160000
UID:0d0a93f0-4fef-4532-bd72-54e2b1baf594
END:VEVENT
END:VCALENDAR
"#;
        let err = load_ics(TEST_ICS_BODY).unwrap_err();
        assert_eq!(
            err.0,
            "Time zone id 'Europe/Berlin' has daylight and standard but a missing recurrence rule"
        );
    }
}
