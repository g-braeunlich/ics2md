[![pipeline status](https://gitlab.com/g-braeunlich/ics2md/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/g-braeunlich/ics2md/-/commits/main)
[![Checked with clippy](https://img.shields.io/badge/clippy-checked-blue)](https://doc.rust-lang.org/stable/clippy/usage.html)
[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# ics2md

CLI tool to fetch an ics calendar from the internet and generate markdown files.

## Usage

```sh
./ics2md.py --md-folder <FOLDER> --calendar-url <URL>
```

The script will fetch the ics calendar specified by `<URL>`.
Markdown files will be generated in `<FOLDER>`.

The `md` files are designed for the static content manager
[zola](https://www.getzola.org/) and will look like:

```md
+++
title = "Round House Kick Workout"
[extra]
start = "2023-04-15T13:00:00+02:00"
end = "2023-04-15T16:00:00+02:00"
location = "Attendees will meet in Texas but might be kicked to another location."
organizer = "Chuck Norris"
uid = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
+++

Description of course
```
